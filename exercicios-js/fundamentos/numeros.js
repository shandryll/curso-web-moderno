const peso1 = 1.0
console.log(peso1)

const peso2 = Number('2.5')
console.log(peso2)

console.log(Number.isInteger(peso1))
console.log(Number.isInteger(peso2))

const avaliacao1 = 9.876
const availacao2 = 6.543

const total = avaliacao1 * peso1 + availacao2 * peso2
const media = total / (peso1 + peso2)

console.log(media)

// Limitando o valor da casa decimal para 4
console.log(media.toFixed(4))

// Transformando o número em texto
console.log(media.toString())

// Pegando o tipo da constante
console.log(typeof media)

console.log(typeof Number)

// Transformado em número binário
console.log(media.toString(2))

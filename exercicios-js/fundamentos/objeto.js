const prod1 = {}
prod1.nome = 'Celular Ultra Mega'
prod1.preco = 4998.90
prod1.desconto = 0.15
console.log(prod1)

const prod2 = {
    nome: 'Camisa Polo',
    preco: 79.90,
    desconto: 0.05,
    especificacoes: {
        cor: 'Vermelho'
    }
}
console.log(prod2)
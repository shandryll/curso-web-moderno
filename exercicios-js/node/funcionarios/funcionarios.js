const url = 'http://files.cod3r.com.br/curso-js/funcionarios.json'
const axios = require('axios')

axios.get(url).then(response => {
    const funcionarios = response.data
    
    // TODO: Qual mulher chinesa com o menor salário?

    const chines = funcionario => funcionario.pais == "China"
    const mulheres = funcionario => funcionario.genero == "F"
    const mulheresChinesas = funcionarios.filter(chines).filter(mulheres)

    const menorSalario =  mulheresChinesas.reduce(function (funcionario, funcionarioAtual) {
        return funcionario.salario < funcionarioAtual.salario ? funcionario : funcionarioAtual
    })
    console.log(menorSalario)

})
// nao aceita repetição/não indexada
const times = new Set()
times.add('Galo')
times.add('Coritiba')
times.add('Palmeiras')
times.add('Galo')
times.add('America')

console.log(times)
console.log(times.size)
console.log(times.has('palmeiras'))
console.log(times.has('Palmeiras'))
times.delete('Palmeiras')
console.log(times.has('Palmeiras'))
console.log(times)

const nomes = ['Raquel', 'Lucas', 'Julia', 'Lucas']
const nomesSet = new Set(nomes)
console.log(nomesSet)
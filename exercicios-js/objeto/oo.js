// CÓDIGO NÃO EXECUTÁVEL!

// procedural
processamento(valor1, valor2, valor3)

// OO
objeto = {
    valor1,
    valor2,
    valor3,
    processamento() {
        // ...
    }
}
objeto.processamento()

// Principios importantes
// 1. Abstração
// 2. Encapisulamento
// 3. Herança (prototype)
// 4. Polimorfismo
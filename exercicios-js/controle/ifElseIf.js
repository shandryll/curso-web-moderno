Number.prototype.entre = function(inicio, fim) {
    return this >= inicio && this <= fim
}

const imprimirResultado = function(nota) {
    if(nota.entre(9, 10)) {
        console.log('Diamante')
    } else if(nota.entre(7, 8.99)) {
        console.log('Platina')
    } else if(nota.entre(5, 6.99)) {
        console.log('Ouro')
    } else if (nota.entre(3, 4.99)) {
        console.log('Prata')
    } else if (nota.entre(1, 2.99)) {
        console.log('Bronze')
    } else if (nota.entre(0, 0.99)) {
        console.log('Cobre')
    } else {
        console.log('Rank inválido')
    }
}

imprimirResultado(12)
imprimirResultado(10)
imprimirResultado(8.9)
imprimirResultado(6.55)
imprimirResultado(3.20)
imprimirResultado(1)
imprimirResultado(0.4)